//!
#![warn(missing_debug_implementations, rust_2018_idioms, missing_docs)]

///
/// My first documentation
///
#[derive(Debug)]
pub struct StrSplit<'h, 'd> {
  // multiple lifetimes are rare
  // this allows delimiter to have a shorter lifetime than haystack
  remainder: &'h str,
  delimiter: &'d str,
}

impl<'h, 'd> StrSplit<'h, 'd> {
  ///
  /// Creates a strsplit object that can be iterated over the splits
  ///
  pub fn new(haystack: &'h str, delimiter: &'d str) -> Self {
    Self {
      remainder: haystack,
      delimiter,
    }
  }
}

//    v-- generic lifetime for this impl
impl<'h, 'd> Iterator for StrSplit<'h, 'd> {
  type Item = &'h str;
  fn next(&mut self) -> Option<Self::Item> {
    if let Some(next_delim) = self.remainder.find(self.delimiter) {
      let until_delimiter = &self.remainder[..next_delim];
      self.remainder = &self.remainder[(next_delim + self.delimiter.len())..];
      Some(until_delimiter)
    } else if self.remainder.is_empty() {
      // CONSIDER: return an empty string if haystack ends with delimeter. I think NOT.
      None
    } else {
      let rest = self.remainder;
      self.remainder = ""; // has type &'static str which is lifetime of program
      Some(rest)
    }
  }
}

#[cfg(test)]
mod tests {
  use super::StrSplit;

  #[test]
  fn tail() {
    let haystack = "a b c d ";
    let letters_coll: Vec<_> = StrSplit::new(haystack, " ").collect();
    assert_eq!(letters_coll, vec!["a", "b", "c", "d"]);
  }

  #[test]
  fn it_works_1() {
    let haystack = "a b c d e";
    let letters_coll: Vec<_> = StrSplit::new(haystack, " ").collect();
    assert_eq!(letters_coll, vec!["a", "b", "c", "d", "e"]);
  }

  #[test]
  fn it_works_2() {
    let haystack = "a b c d e";
    let letters = StrSplit::new(haystack, " ");
    assert!(letters.eq(vec!["a", "b", "c", "d", "e"]));
  }
}
