pub fn print_stuff() {
  // print to console
  println!("Hello from print rs file");

  // Basic Formatting
  println!("Number: {}", 1);

  // Positional Parameters
  println!(
    "{0} is from {1} and {0} likes to {2}",
    "Brad", "Mass", "code"
  );

  println!(
    "{name} likes to play {activity}",
    activity = "baseball",
    name = "Rob"
  );

  println!("Binary: {0:b} Hex {0:X}", 10);

  // Placedholder for debug trait
  println!("{:?}", (12, true, "hello"));

  // Basic math
  println!("answer = {}", 10 + 10);

  #[derive(Debug)]
  struct RobData {
    pub a: i32,
    pub b: f32,
  }

  let data = RobData { a: 1, b: 1.1 };
  println!("Rob Data: {:#?}", data); // pretty print of default print
  println!("Rob Data: {:?}", data);
}
