// Enums are types which have a few definite values
#![allow(dead_code)]

#[derive(Debug)]
struct Point {
  x: i32,
  y: i32,
}
#[derive(Debug)]
enum Direction {
  // Variants
  Up(Point),
  Down(Point),
  Left(Point),
  Right(Point),
}
#[derive(Debug)]
enum Keys {
  UpKey(String),
  DownKey(String),
  LeftKey(String),
  RightKey(String),
}

impl Direction {
  fn match_direction(&self) -> Keys {
    match *self {
      // must deref self here
      Direction::Up(_) => Keys::UpKey(String::from("Pressed w")),
      Direction::Down(_) => Keys::UpKey(String::from("Pressed s")),
      Direction::Left(_) => Keys::UpKey(String::from("Pressed a")),
      Direction::Right(_) => Keys::UpKey(String::from("Pressed d")),
    }
  }
}

impl Keys {
  fn get_destruct(&self) -> &String {
    match *self {
      Keys::UpKey(ref s) => s,
      Keys::DownKey(ref s) => s,
      Keys::LeftKey(ref s) => s,
      Keys::RightKey(ref s) => s,
    }
  }
}

enum Shape {
  Rectangle { width: u32, height: u32 },
  Square(u32),
  Circle(f64),
}

impl Shape {
  fn area(&self) -> f64 {
    match *self {
      Shape::Rectangle { width, height } => (width * height) as f64,
      Shape::Square(ref s) => (s * s) as f64,
      Shape::Circle(ref r) => 3.14159 * (r * r),
    }
  }
}

fn division(x: f64, y: f64) -> Option<f64> {
  if y == 0.0 {
    None
  } else {
    Some(x / y)
  }
}

pub fn run() {
  let u = Direction::Up(Point { x: 0, y: 1 });
  let k = u.match_direction();
  let x = k.get_destruct();
  println!("{} : {:?}", x, k);

  let r = Shape::Rectangle {
    width: 10,
    height: 700,
  };
  let s = Shape::Square(10);
  let c = Shape::Circle(4.5);
  println!("r area: {}", r.area());
  println!("s area: {}", s.area());
  println!("c area: {}", c.area());
  let shape_vec = vec![s, c];
  shape_vec
    .iter()
    .for_each(|s| println!("area: {}", s.area()));
  match division(5.0, 7.0) {
    Some(x) => println!("division result: {:.10}", x),
    None => println!("Cannot divide by 0",),
  }
}
