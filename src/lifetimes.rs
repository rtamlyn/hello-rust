// lifetime specifier needed if returning a ref input
fn pr<'a>(x: &'a str, y: &'a str) -> &'a str {
  if x.len() == y.len() {
    x
  } else {
    y
  }
}

struct A<'a> {
  x: &'a str, // lifetime needed since struct contains a ref
}

// fn get_a<'a>(s: &'a str) -> &'a str { // lifetime not needed with only parameter
fn get_a(s: &str) -> &str {
  // with only one parameter lifetime of output is inferred
  s
}

impl<'a> A<'a> {
  // &self gives its lifetime to the outputs
  // fn get_x<'b>(&'b self) -> &'b str { // not needed
  fn get_x(&self) -> &str {
    self.x
  }
}

pub fn run() {
  let a = "a string";
  let b = "b string";
  let c = pr(a, b);
  println!("{}", c);
  let a = A { x: "Hello" };
  println!("{}", a.get_x());
  let s: &'static str = "The String"; // special lifetime static
  println!("{}", get_a(s))
}
