trait Shape {
  fn area(&self) -> f64;
}

struct Rect {
  x: u32,
  y: u32,
}

struct Cube {
  x: u32,
  y: u32,
  z: u32,
}

struct Circle {
  radius: f64,
}

impl Shape for Cube {
  fn area(&self) -> f64 {
    (self.x * self.y) as f64
  }
}

impl Shape for Rect {
  fn area(&self) -> f64 {
    (self.x * self.y) as f64
  }
}

impl Shape for Circle {
  fn area(&self) -> f64 {
    ((self.radius * self.radius) as f64) * 3.14159
  }
}

#[derive(Debug)]
struct Vector {
  x: f64,
  y: f64,
  z: f64,
}

use std::ops;
impl ops::Add<Vector> for Vector {
  type Output = Vector;
  fn add(self, rhs: Vector) -> Vector {
    Vector {
      x: self.x + rhs.x,
      y: self.y + rhs.y,
      z: self.z + rhs.z,
    }
  }
}

struct Fib {
  c: u32,
  n: u32,
}

impl Iterator for Fib {
  type Item = u32;
  fn next(&mut self) -> Option<u32> {
    let n = self.c + self.n;
    self.c = self.n;
    self.n = n;
    Some(self.c)
  }
}

fn fib() -> Fib {
  Fib { c: 1, n: 1 }
}

pub fn run() {
  let r = Rect { x: 10, y: 700 };
  let cube = Cube {
    x: 10,
    y: 700,
    z: 1,
  };
  let c = Circle { radius: 4.5 };
  println!("r area: {:.3}", r.area());
  println!("cube area {}: {}", cube.z, cube.area());
  println!("c area: {:.4}", c.area());

  let v1 = Vector {
    x: 1.,
    y: 0.,
    z: 0.,
  };
  let v2 = Vector {
    x: 1.,
    y: 0.,
    z: 1.,
  };
  let v3 = v1 + v2;
  println!("{:?}", v3);
  // println!("{:?}", v2);
  // println!("{:?}", v1);

  for j in fib().take(10) {
    println!("{}", j);
  }
  for j in fib().skip(10).take(10) {
    println!("{}", j);
  }
  // let my_fib = fib();
  let my_fib = Fib { c: 1, n: 1 };
  for j in my_fib.skip(10).take(10) {
    println!("{}", j);
  }
  let mut my_fib2 = Fib { c: 1, n: 1 };
  println!("{:?}", my_fib2.next());
}
