#[allow(unused_variables)]

pub fn run() {
  let a = String::from("Howdy"); // String in the heap
  let str_slice: &str = "There"; // str slices borrow ownership
  let b = a;
  // println!("{}", a); // not OK on stack
  println!("{}", b);

  let heap_vec: Vec<i8> = vec![5, 2];
  let heap_string: String = String::from("there");
  let heap_i8: Box<i8> = Box::new(30); // normally do this with more than i8

  let stack_i8: i8 = 10;
  let stack_i8_2 = stack_i8; // OK rust makes copy on stack since it is cheap to do so
  println!("{}", stack_i8);
  println!("{}", stack_i8_2);

  let heap_i8_2 = heap_i8; // ownership goes to heap_i8_2
                           // println!("{}", heap_i8); // not OK
  println!("{}", heap_i8_2);

  let stack_f64 = 1.0;
  let heap_f64 = Box::new(2.0);

  stack_proc(stack_f64); // creates a copy; copy cheap
  println!("In main stack {}", stack_f64); // stack_f64 is not modified

  // heap_procedure(heap_f64); // copy expensive; takes ownership
  heap_procedure(&heap_f64); // borrows ownership with ref
  println!("In main heap {}", heap_f64); // heap_f64 ownership returned
}

fn stack_proc(mut p: f64) {
  // copy is modified
  p += 9.0;
  println!("In stack proc {}", p)
}

// fn heap_procedure(p: Box<f64>) { // takes ownership
fn heap_procedure(p: &Box<f64>) {
  // uses ref
  // (*p) += 42.;
  println!("In heap proc {}", p)
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn it_works() {
    run();
    assert_eq!(2 + 2, 4);
  }
}
