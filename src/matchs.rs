// match pattern matches

pub fn run() {
  let n = 19;
  match n {
    1 => println!("This is one"),
    2 | 3 | 5 | 7 | 11 => println!("This is prime"),
    13..=19 => println!("A teen",),
    _ => println!("Ain't special",),
  }
  let pair = (0, -2);
  match pair {
    (0, y) => println!("y: {}", y),
    (x, 0) => println!("x: {}", x),
    _ => println!("No Match",),
  }
  let pair = (-2, -2);
  match pair {
    (x, y) if x == y => println!("x and y are equal: {}", y),
    (0, y) => println!("y: {}", y),
    (x, 0) => println!("x: {}", x),
    _ => println!("No Match",),
  }
  let p = 12;
  match p {
    n @ 1..=12 => println!("p is 1 to 12: {}", n),
    n @ 13..=19 => println!("p is 13 to 19: {}", n),
    _ => println!("No Match",),
  }
  let p = 13;
  let x = match p {
    n @ 1..=12 => n,
    n @ 13..=19 => n * 4,
    _ => 0,
  };
  println!("X result: {}", x)
}
