enum List {
  Cons(i32, Box<List>),
  End,
}

use List::Cons;
use List::End;

fn is_even(n: u32) -> bool {
  n % 2 == 0
}

pub fn run() {
  let b = Box::new(10);
  println!("b: {}", b);

  let _l = Cons(1, Box::new(Cons(2, Box::new(End))));
  let s: u32 = (0..)
    .map(|n| n * n)
    .take_while(|&n| n < 10000)
    .filter(|&n| is_even(n))
    .fold(0, |s, i| s + i);
  println!("{}", s);

  let vec: Vec<_> = vec![0, 1, 2, 3];
  vec.iter().for_each(|v| println!("{}", v));
  vec.iter().for_each(|v| println!("{}", v));
}
