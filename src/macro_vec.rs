// replicate macro for creating vec
// see the little book of rust macros
use std::vec;

pub fn run() {
  let numbers: Vec<f32> = vec![1.0, 2.0, 3f32, 4.0];
  println!("Result: {}", "Hi there");
  println!("number[2]: {}", numbers[2]);
}
