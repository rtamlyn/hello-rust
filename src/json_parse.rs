extern crate serde;
extern crate serde_json;

// #[macro_use]
extern crate serde_derive;
use serde_derive::{Deserialize, Serialize};

use serde_json::Value as JsonValue;

#[derive(Serialize, Deserialize)]
struct Person {
  name: String,
  age: i32,
  lucky_husband: bool,
}

pub fn run() {
  let json_str = r#"
  {
    "name": "Rob",
    "age": 69,
    "lucky_husband": true
  }
  "#;
  let res0 = serde_json::from_str(json_str);
  //
  if res0.is_ok() {
    let j: JsonValue = res0.unwrap();
    println!("{}", j["name"]);
    println!("{}", j["name"].as_str().unwrap());
  } else {
    println!("{}", "Error in JSON");
  }
  // if res0.is_ok() {
  //   let p: Person = res0.unwrap();
  //   println!("{}", p.name);
  // } else {
  //   println!("{}", "Error in JSON");
  // }
  let res1: std::result::Result<Person, serde_json::Error> = serde_json::from_str(json_str);
  match res1 {
    Ok(p) => println!("{}", p.name),
    Err(e) => println!("{}", e),
  }
}
