fn get_occupation(name: &str) -> Option<&str> {
  match name {
    "Robert" => Some("Software guru"),
    "Ralph" => Some("Little r"),
    _ => None,
  }
}

pub fn run() {
  let name = String::from("Robert");
  match name.chars().nth(3) {
    Some(c) => println!("{}", c),
    None => println!("Char not found"),
  };
  let occ = get_occupation("Robert");
  match occ {
    Some(o) => println!("{}", o),
    None => println!("Not found"),
  }
  match get_occupation("Ralph") {
    Some(o) => println!("{}", o),
    None => println!("Not found"),
  }
}
