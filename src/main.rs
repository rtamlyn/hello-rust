use std::collections::HashMap;

// mod macro_vec;
// use macro_vec::*;

// mod timer;
// use timer::Timer;

// // mod gui;

// // extern crate utils;
// #[macro_use]
// extern crate vecmac;

// #[macro_use]
// extern crate log;
// extern crate simple_logger;

// extern crate strsplit;
// use strsplit::StrSplit;

fn main() {
    // let v: Vec<i32> = avec!();
    // let _result = simple_logger::init();
    // info!("Some info about v length: {}", v.len());
    // warn!("Warning about v length: {}", v.len());
    // error!("An error about v length: {}", v.len());
    // let _t = Timer::new("Main really");

    // let my_skills = "typescript angular rust";
    // let letters_coll: Vec<_> = StrSplit::new(my_skills, " ").collect();
    // println!("skills: {:?}", letters_coll);

    // run();

    let vec = vec![0, 1, 2, 3];
    let vec_2: Vec<_> = vec.iter().map(|x| x * 2).collect();
    let map: HashMap<_, _> = vec_2.iter().map(|x| x * 2).enumerate().collect();
    dbg!(map.len());
    dbg!(map);

    let mut iter_vec = (&vec).into_iter();
    while let Some(v) = iter_vec.next() {
        dbg!(v);
    }
    // same but cleaner
    for v in &vec {
        dbg!(v);
    }

    // Errors:
    // Recover
    // Rethrow
    // panic
    // dont use my own result type
    // but do alias the Error Type
    pub struct MyErr {
        // description: &'a str,
        description: String,
    }
    type MyResult<T> = Result<T, MyErr>;

    fn verify_pos(i: i32) -> MyResult<i32> {
        if i > 0 {
            return Ok(i);
        }
        Err(MyErr {
            description: "Not Positive".to_string(),
        })
    }
    match verify_pos(-4) {
        Ok(x) => println!("{}", x),
        Err(e) => println!("{}", e.description),
    }
    let x = match verify_pos(4) {
        Ok(x) => x,
        Err(e) => {
            println!("{}", e.description);
            // -1
            return; // ends main function I think
        }
    };
    println!("not quite done!");
    dbg!(x);
    // let y = verify_pos(8)?;
    ()
}
