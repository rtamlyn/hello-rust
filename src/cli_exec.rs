use std::process::Command;

pub fn run() {
  let mut py_cmd = Command::new("python");
  py_cmd.arg("src/dcode.py");
  match py_cmd.output() {
    // Ok(o) => unsafe { println!("{}", String::from_utf8_unchecked(o.stdout)) },
    Ok(o) => match String::from_utf8(o.stdout) {
      Ok(s) => println!("{}", s),
      Err(e) => println!("Error: {}", e),
    },
    Err(e) => println!("Error happened to happen: {}", e),
  }
}
