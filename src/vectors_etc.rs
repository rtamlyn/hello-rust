use std::collections::HashMap;
use std::fs::File;

pub fn run() {
  // let mut v = Vec::new();
  let mut v: Vec<i32> = Vec::new();
  println!("{:?} {} {}", &v, v.len(), v.capacity());
  v.push(5);
  println!("{:?} {} {}", &v, v.len(), v.capacity());
  v.push(5);
  v.push(5);
  v.push(5);
  v.push(4);
  println!("{:?} {} {}", &v, v.len(), v.capacity());
  println!("Result of pop: {:?}", v.pop());

  // let mut hm = HashMap::new();
  let mut hm: HashMap<String, i32> = HashMap::new();
  hm.insert(String::from("random"), 12);
  hm.insert(String::from("the answer"), 42);

  for (k, v) in &hm {
    println!("{}: {}", k, v)
  }

  let s = hm.get(&String::from("the answer"));
  if let Some(n) = s {
    println!("if let: {}", n)
  } else {
  }

  hm.remove(&String::from("random"));
  match hm.get(&String::from("random")) {
    Some(n) => println!("{}", n),
    _ => println!("No match",),
  }

  let f = 24.4321_f32;
  let i = f as u8;
  let c = (i + 23) as char;
  println!("{} {} {}", f, i, c);

  let f = File::open("Cargoe.toml");
  let _f = match f {
    Ok(file) => file,
    Err(error) => panic!("There was a problem with file: {:?}", error),
  };
}
