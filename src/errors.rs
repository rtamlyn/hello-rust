use std::fs::File;
use std::io::Error;
use std::io::Read;

fn read_file() -> Result<String, Error> {
  let f = File::open("src/gui.rs");
  let mut f = match f {
    Ok(file) => file,
    Err(e) => return Err(e),
  };
  let mut s = String::new();
  match f.read_to_string(&mut s) {
    Ok(_) => Ok(s),
    Err(e) => Err(e),
  }
}

fn read_file_2() -> Result<String, Error> {
  let mut s = String::new();
  let mut f = File::open("src/gui.rs")?;
  f.read_to_string(&mut s)?;
  Ok(s)
}

fn read_file_3() -> Result<String, Error> {
  let mut s = String::new();
  File::open("src/gui.rs")?.read_to_string(&mut s)?;
  Ok(s)
}

pub fn run() {
  match read_file() {
    Ok(s) => println!("{}", s),
    Err(e) => println!("{:?}", e),
  }
  match read_file_2() {
    Ok(s) => println!("{}", s),
    Err(e) => println!("{:?}", e),
  }
  match read_file_3() {
    Ok(s) => println!("{}", s),
    Err(e) => println!("{:?}", e),
  }
}
