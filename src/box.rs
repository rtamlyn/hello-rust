// See Chapter 17 and 15
// Smart Pointers

// Traditional Struct
struct Color {
  red: u8,
  green: u8,
  blue: u8,
}

use std::ops::Deref;

struct MyBox<T>(T);

impl<S> MyBox<S> {
  fn new(x: S) -> MyBox<S> {
    MyBox(x)
  }
}

impl<T> Deref for MyBox<T> {
  type Target = T;
  fn deref(&self) -> &T {
    &self.0
  }
}

impl<T> Drop for MyBox<T> {
  fn drop(&mut self) {
    println!("Dropping MyBox");
  }
}

pub fn run() {
  let c = Color {
    red: 128,
    green: 128,
    blue: 128,
  };

  // A smart pointer (String is also a smart pointer; str is not)
  let b = Box::new(Color {
    red: 0,
    green: 128,
    blue: 128,
  });

  let my_box = MyBox::new(Color {
    red: 255,
    green: 128,
    blue: 128,
  });
  // let b = Box::new(5);
  println!("RGB {} {} {}", c.red, c.green, c.blue);
  println!("RGB {} {} {}", b.red, b.green, b.blue);
  assert_eq!(c.blue, b.blue);
  assert_eq!(c.blue, my_box.blue);
}
