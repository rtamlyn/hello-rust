#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

// #![allow(unused_variables)]
// fn main() {
pub trait Draw {
    fn draw(&self);
}

fn foo(input: Option<i32>) -> Option<i32> {
    if input.is_none() {
        return None;
    }
    let input = input.unwrap();
    if input < 0 {
        return None;
    }
    Some(input)
}

fn foo_better(input: Option<i32>) -> Option<i32> {
    input.and_then(|i| {
        if i > 0 {
            return Some(i);
        }
        None
    })
}

pub struct Screen {
    pub components: Vec<Box<dyn Draw>>,
}

impl Screen {
    pub fn run(&self) {
        for component in self.components.iter() {
            component.draw();
        }
    }
}

pub struct Button {
    pub width: u32,
    pub height: u32,
    pub label: String,
}

impl Draw for Button {
    fn draw(&self) {
        // code to actually draw a button
    }
}

// }
