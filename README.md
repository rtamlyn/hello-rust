# Rust Tutorial Notes

## Setup

- install rust

```bash
cargo new hello-rust # new project hello-rust
cargo run
cargo build
cargo build --release

# install expand sub command
cargo install cargo-expand # does not work on normal rust need nightly builds
# cargo install rustfmt # not done yet
# pip install Pygments # not done yet

# create a lib in this project
cargo new --lib utils
cargo new --lib strsplit

rustup update stable # updates rust

rustup component add rustfmt
```

- git

```zsh
git reset fa57c9a --soft; git ci -a -m "Adds strsplit"
```
